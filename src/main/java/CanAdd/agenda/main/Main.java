package CanAdd.agenda.main;

import CanAdd.agenda.presentacion.controlador.Controlador;
import CanAdd.agenda.presentacion.vista.Advertiser;
import CanAdd.agenda.presentacion.vista.VentanaConfiguracion;
import CanAdd.agenda.presentacion.vista.Vista;


public class Main {

    public static void main(String[] args) {
        LaunchValidator validator = new LaunchValidator();
        if (validator.isFirstLaunch()) {
            //SE ABRE PANTALLA DE CONFIG
            Advertiser.advertencia("Primer lanzamiento de la aplicacion. Configure la base de datos");
            VentanaConfiguracion ventanaConfiguracion = new VentanaConfiguracion();
            validator.changeFirstLaunch();
        }
        else {
            try {
                Vista vista = new Vista();
                Controlador controlador = new Controlador(vista);
                controlador.inicializar();
            } catch (Exception e) {
                Advertiser.advertencia("Se ha producido un error en la base de datos. Una vez terminado," +
                        " renicie la aplicación");
                VentanaConfiguracion ventanaConfiguracion = new VentanaConfiguracion();
            }
        }

    }
}
