package CanAdd.agenda.main;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class LaunchValidator {

    private Properties properties;

    private FileInputStream input;
    private FileOutputStream output;

    public LaunchValidator() {
        this.properties = new Properties();
    }

    public boolean isFirstLaunch(){
        openInputFile();
        String flag = getPropertyValue("firstLaunch");
        closeFile(input);
        return Boolean.parseBoolean(flag);
    }

    public void changeFirstLaunch(){
        openOutputFile();
        setPropertyValue("firstLaunch", "false");
        closeFile(output);
    }

    private String getPropertyValue(String key) {
        String ret = properties.getProperty(key);
        return ret;
    }

    private void setPropertyValue(String key, String value) {
        properties.setProperty(key, value);
        try {
            properties.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openInputFile() {
        try {
            input = new FileInputStream("config.properties");
            properties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openOutputFile() {
        try {
            output = new FileOutputStream("config.properties");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeFile(Closeable file) {
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
