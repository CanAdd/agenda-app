package CanAdd.agenda.dto;

public class LocalidadDTO {
    private int idLocalidad;
    private String localidad;

    public LocalidadDTO(int id, String localidad) {
        this.idLocalidad = id;
        this.localidad = localidad;
    }

    public int getIdLocalidad() {
        return idLocalidad;
    }

    public String getLocalidad() {
        return localidad;
    }


    @Override
    public String toString() {
        return localidad;
    }

}
