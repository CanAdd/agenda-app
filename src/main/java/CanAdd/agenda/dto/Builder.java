package CanAdd.agenda.dto;

public interface Builder<T> {

    public T build();
}
