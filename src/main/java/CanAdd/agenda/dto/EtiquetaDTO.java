package CanAdd.agenda.dto;


public class EtiquetaDTO {

    private int idEtiqueta;
    private String relacion;

    public EtiquetaDTO(int idEtiqueta, String relacion) {
        this.idEtiqueta = idEtiqueta;
        this.relacion = relacion;
    }

    public String getRelacion() {
        return relacion;
    }

    public int getIdEtiqueta() {
        return idEtiqueta;
    }

    @Override
    public String toString() {
        return relacion;
    }

}