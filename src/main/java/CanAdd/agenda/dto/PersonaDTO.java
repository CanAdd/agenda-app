package CanAdd.agenda.dto;


public class PersonaDTO {
    private int idPersona;
    private String nombre;
    private String cumpleAños;
    private String horoscopo;
    private DireccionDTO direccion;
    private LocalidadDTO localidad;
    private EtiquetaDTO etiqueta;
    private MediosDeContactosDTO mediosDeContacto;

    private PersonaDTO(PersonaBuilder builder) {
        this.idPersona = builder.idPersona;
        this.nombre = builder.nombre;

        this.cumpleAños = builder.cumpleAños;

        this.direccion = new DireccionDTO(builder.idPersona, builder.calle, builder.numero, builder.depto,
                builder.piso);

        this.localidad = builder.localidad;

        this.etiqueta = builder.etiquetaDTO;

        this.mediosDeContacto = new MediosDeContactosDTO(builder.idPersona, builder.telefono, builder.email);

    }

    public static class PersonaBuilder implements Builder<PersonaDTO> {

        //Parametros requeridos
        private int idPersona;
        private String nombre;
        private String cumpleAños;
        //Medios de Contactos;
        private String telefono;

        //Parámetro opcionales

        //Direcciones
        private String calle;
        private String numero;
        private String depto;
        private String piso;

        //Localidades
        private LocalidadDTO localidad;

        //Etiquetas
        private EtiquetaDTO etiquetaDTO;

        //Medios de Contactos
        private String email;

        public PersonaBuilder(int idPersona, String nombre, String telefono) {
            this.idPersona = idPersona;
            this.nombre = nombre;
            this.telefono = telefono;
        }

        public PersonaBuilder withDireccion(String calle, String numero, String depto
                , String piso) {
            this.calle = calle;
            this.numero = numero;
            this.depto = depto;
            this.piso = piso;
            return this;
        }

        public PersonaBuilder withLocalidad(LocalidadDTO localidad) {
            this.localidad = localidad;

            return this;
        }

        public PersonaBuilder withEtiqueta(EtiquetaDTO etiqueta) {
            this.etiquetaDTO = etiqueta;

            return this;
        }

        public PersonaBuilder withContacto(String email) {
            this.email = email;

            return this;
        }

        public PersonaBuilder withCumpleaños(String fecha) {
            this.cumpleAños = fecha;

            return this;
        }

        public PersonaDTO build() {
            return new PersonaDTO(this);
        }

    }

    public int getIdPersona() {
        return this.idPersona;
    }

    public String getNombre() {
        return this.nombre;
    }

    public DireccionDTO getDireccion() {
        return direccion;
    }

    public LocalidadDTO getLocalidad() {
        return localidad;
    }

    public EtiquetaDTO getEtiqueta() {
        return etiqueta;
    }

    public MediosDeContactosDTO getMediosDeContacto() {
        return mediosDeContacto;
    }

    public String getHoroscopo() {
        return horoscopo;
    }

    public void setHoroscopo(String horoscopo) {
        this.horoscopo = horoscopo;
    }

    public String getCumpleaños() {
        return cumpleAños;
    }

}
