package CanAdd.agenda.dto;

public class ConexionSettingsDTO {
    private String admin;
    private String password;
    private String port;
    private String ip;

    public ConexionSettingsDTO(String admin, String password, String port, String ip) {
        this.admin = admin;
        this.password = password;
        this.port = port;
        this.ip = ip;
    }

    public String getAdmin() {
        return admin;
    }

    public String getPassword() {
        return password;
    }

    public String getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }
}
