package CanAdd.agenda.dto;

public class MediosDeContactosDTO {

    private int idPersona;
    private String telefono;
    private String email;

    MediosDeContactosDTO(int idPersona, String telefono, String email) {
        this.idPersona = idPersona;
        this.telefono = telefono;
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public int getIdPersona() {
        return idPersona;
    }
}
