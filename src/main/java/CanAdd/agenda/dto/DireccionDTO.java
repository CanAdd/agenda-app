package CanAdd.agenda.dto;

public class DireccionDTO {
    private int personaId;
    private String calle;
    private String numero;
    private String depto;
    private String piso;


    DireccionDTO(int idPersona, String calle, String numero, String depto, String piso) {
        this.personaId = idPersona;
        this.calle = calle;
        this.personaId = idPersona;
        this.numero = numero;
        this.depto = depto;
        this.piso = piso;
    }


    public String getPiso() {
        return piso;
    }

    public String getDepto() {
        return depto;
    }

    public String getNumero() {
        return numero;
    }

    public String getCalle() {
        return calle;
    }

    public void setPersonaId(int personaId) {
        this.personaId = personaId;
    }
}
