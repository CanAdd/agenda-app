package CanAdd.agenda.modelo;

import java.sql.SQLException;
import java.util.List;

import CanAdd.agenda.dto.PersonaDTO;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;
import CanAdd.agenda.persistencia.dao.mysql.PersonaDAOImpl;

public class Agenda {
    private DAO<PersonaDTO> persona;

    public Agenda() {
        persona = new PersonaDAOImpl();
    }

    public void agregarPersona(PersonaDTO nuevaPersona) {
        persona.insert(nuevaPersona);
    }

    public void editarPersona(PersonaDTO nuevaPersona) {
        persona.upgrade(nuevaPersona);
    }

    public void borrarPersona(PersonaDTO persona_a_eliminar) throws SQLException {
        persona.delete(persona_a_eliminar);
    }

    public List<PersonaDTO> obtenerPersonas() {

        List<PersonaDTO> ret = persona.readAll();
        ret.forEach(p -> p.setHoroscopo(Persona.getSigno(p)));

        return ret;
    }

    public List<PersonaDTO> sortByHroscopo(List<PersonaDTO> personas) {
        personas.sort((p1, p2) -> {
            int ret1 = p1.getHoroscopo().compareTo(p2.getHoroscopo());

            if (ret1 == 0)
                return p1.getNombre().compareTo(p2.getNombre());

            return ret1;
        });

        return personas;
    }

}
