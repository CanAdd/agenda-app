package CanAdd.agenda.modelo;

import CanAdd.agenda.dto.ConexionSettingsDTO;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ConexionSettings {

    private Properties properties;
    private FileInputStream input;
    private FileOutputStream output;

    public ConexionSettings() {
        properties = new Properties();
    }

    public ConexionSettingsDTO getSettings() {
        openInputFile();

        String admin = getPropertyValue("admin");
        String password = getPropertyValue("password");
        String port = getPropertyValue("port");
        String ip = getPropertyValue("ip");

        closeFile(input);

        return new ConexionSettingsDTO(admin, password, port, ip);
    }

    public void setSettings(ConexionSettingsDTO settings) {
        openOutputFile();

        setPropertyValue("admin", settings.getAdmin());
        setPropertyValue("password", settings.getPassword());
        setPropertyValue("port", settings.getPort());
        setPropertyValue("ip", settings.getIp());

        try {
            properties.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeFile(output);


    }

    private String getPropertyValue(String key) {
        return properties.getProperty(key);
    }

    private void setPropertyValue(String key, String value) {
        properties.setProperty(key, value);
    }

    private void openInputFile() {
        try {
            input = new FileInputStream("database.properties");
            properties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openOutputFile() {
        try {
            output = new FileOutputStream("database.properties");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeFile(Closeable file) {
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
