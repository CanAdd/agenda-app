package CanAdd.agenda.modelo;

import CanAdd.agenda.dto.EtiquetaDTO;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;
import CanAdd.agenda.persistencia.dao.mysql.EtiquetaDAOImpl;

import java.sql.SQLException;
import java.util.List;

public class EtiquetaService {

    private DAO<EtiquetaDTO> etiqueta;

    public EtiquetaService() {
        this.etiqueta = new EtiquetaDAOImpl();
    }

    public void agregarEtiqeta(EtiquetaDTO etiqueta) {
        this.etiqueta.insert(etiqueta);
    }

    public void eliminarEtiqueta(EtiquetaDTO etiqueta) throws SQLException {
        this.etiqueta.delete(etiqueta);
    }

    public List<EtiquetaDTO> readAllEtiquetas() {
        return this.etiqueta.readAll();
    }
}
