package CanAdd.agenda.modelo;

import CanAdd.agenda.dto.PersonaDTO;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


public class Persona {

    public static String getSigno(PersonaDTO persona) {
        DateTime date = DateTimeFormat.forPattern("dd/MM").parseDateTime(persona.getCumpleaños());

        if ((date.getMonthOfYear() == 12 && date.getDayOfMonth() >= 22 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 1 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 19))
            return Signo.CAPRICORNIO.toString();
        else if ((date.getMonthOfYear() == 1 && date.getDayOfMonth() >= 20 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 2 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 17))
            return Signo.ACUARIO.toString();
        else if ((date.getMonthOfYear() == 2 && date.getDayOfMonth() >= 18 && date.getDayOfMonth() <= 29) || (date.getMonthOfYear() == 3 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 19))
            return Signo.PISCIS.toString();
        else if ((date.getMonthOfYear() == 3 && date.getDayOfMonth() >= 20 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 4 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 19))
            return Signo.ARIES.toString();
        else if ((date.getMonthOfYear() == 4 && date.getDayOfMonth() >= 20 && date.getDayOfMonth() <= 30) || (date.getMonthOfYear() == 5 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 20))
            return Signo.TAURO.toString();
        else if ((date.getMonthOfYear() == 5 && date.getDayOfMonth() >= 21 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 6 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 20))
            return Signo.GEMINIS.toString();
        else if ((date.getMonthOfYear() == 6 && date.getDayOfMonth() >= 21 && date.getDayOfMonth() <= 30) || (date.getMonthOfYear() == 7 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 22))
            return Signo.CANCER.toString();
        else if ((date.getMonthOfYear() == 7 && date.getDayOfMonth() >= 23 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 8 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 22))
            return Signo.LEO.toString();
        else if ((date.getMonthOfYear() == 8 && date.getDayOfMonth() >= 23 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 9 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 22))
            return Signo.VIRGO.toString();
        else if ((date.getMonthOfYear() == 9 && date.getDayOfMonth() >= 23 && date.getDayOfMonth() <= 30) || (date.getMonthOfYear() == 10 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 22))
            return Signo.LIBRA.toString();
        else if ((date.getMonthOfYear() == 10 && date.getDayOfMonth() >= 23 && date.getDayOfMonth() <= 31) || (date.getMonthOfYear() == 11 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 21))
            return Signo.ESCORPIO.toString();
        else if ((date.getMonthOfYear() == 11 && date.getDayOfMonth() >= 22 && date.getDayOfMonth() <= 30) || (date.getMonthOfYear() == 12 && date.getDayOfMonth() >= 1 && date.getDayOfMonth() <= 21))
            return Signo.SAGITARIO.toString();
        else
            throw new IllegalArgumentException("Fecha invalida " + date.toString());
    }


}
