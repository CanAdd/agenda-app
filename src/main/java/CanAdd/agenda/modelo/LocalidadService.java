package CanAdd.agenda.modelo;

import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;
import CanAdd.agenda.persistencia.dao.mysql.LocalidadDAOImpl;

import java.sql.SQLException;
import java.util.List;

public class LocalidadService {

    private DAO<LocalidadDTO> localidad;

    public LocalidadService() {
        this.localidad = new LocalidadDAOImpl();
    }


    public void agregarLocalidad(LocalidadDTO localidad) {
        this.localidad.insert(localidad);
    }

    public void eliminarLocalidad(LocalidadDTO localidad) throws SQLException {
        this.localidad.delete(localidad);
    }

    public List<LocalidadDTO> readAllLocalidades() {
        return this.localidad.readAll();
    }


}

