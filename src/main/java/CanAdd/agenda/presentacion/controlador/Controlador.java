package CanAdd.agenda.presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import CanAdd.agenda.dto.EtiquetaDTO;
import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.modelo.Agenda;
import CanAdd.agenda.modelo.EtiquetaService;
import CanAdd.agenda.modelo.LocalidadService;
import CanAdd.agenda.presentacion.reportes.ReporteAgenda;
import CanAdd.agenda.presentacion.vista.*;
import CanAdd.agenda.dto.PersonaDTO;

public class Controlador implements ActionListener {

    private List<PersonaDTO> personas_en_tabla;

    private Vista vista;
    private VentanaPersona ventanaPersona;
    private VentanaPersonaEdicion ventanaPersonaEdicion;
    private VentanaLocalidades ventanaLocalidades;
    private VentanaEtiquetas ventanaEtiquetas;
    private VentanaConfiguracion ventanaConfiguracion;

    private LoggingValidator validator = LoggingValidator.getInstance();

    private EtiquetaService etiquetaService;
    private LocalidadService localidadService;
    private Agenda agenda;


    public Controlador(Vista vista) {
        this.vista = vista;
        this.vista.getBtnAgregar().addActionListener(this);
        this.vista.getBtnEditar().addActionListener(this);
        this.vista.getBtnBorrar().addActionListener(this);
        this.vista.getBtnReporte().addActionListener(this);
        this.vista.getBtnLocalidades().addActionListener(this);
        this.vista.getBtnEtiquetas().addActionListener(this);
        this.vista.getBtnConfiguracion().addActionListener(this);
        this.personas_en_tabla = null;

        agenda = new Agenda();
        etiquetaService = new EtiquetaService();
        localidadService = new LocalidadService();
    }

    public void inicializar() {
        this.llenarTabla();
    }

    private void llenarTabla() {
        this.vista.getModelPersonas().setRowCount(0); //Para vaciar la tabla
        this.vista.getModelPersonas().setColumnCount(0);
        this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());

        this.personas_en_tabla = agenda.obtenerPersonas();

        for (PersonaDTO aPersonas_en_tabla : this.personas_en_tabla) {
            System.out.println(aPersonas_en_tabla.getEtiqueta().getRelacion());
            Object[] fila = {

                    aPersonas_en_tabla.getNombre(),
                    aPersonas_en_tabla.getMediosDeContacto().getTelefono(),
                    aPersonas_en_tabla.getDireccion().getCalle(),
                    aPersonas_en_tabla.getDireccion().getNumero(),
                    aPersonas_en_tabla.getDireccion().getPiso(),
                    aPersonas_en_tabla.getDireccion().getDepto(),
                    aPersonas_en_tabla.getLocalidad().getLocalidad(),
                    aPersonas_en_tabla.getMediosDeContacto().getEmail(),
                    aPersonas_en_tabla.getCumpleaños(),
                    aPersonas_en_tabla.getEtiqueta().getRelacion()};

            this.vista.getModelPersonas().addRow(fila);
        }
        this.vista.show();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.vista.getBtnAgregar()) {
            this.ventanaPersona = new VentanaPersona(this);
        } else if (e.getSource() == this.vista.getBtnEditar()) {
            int fila = this.vista.getTablaPersonas().convertRowIndexToModel(this.vista.getTablaPersonas().getSelectedRow());

            if (fila != -1)
                this.ventanaPersonaEdicion = new VentanaPersonaEdicion(this, this.personas_en_tabla.get(fila));
            else
                Advertiser.advertencia("Por favor seleccione un contacto");
        } else if (e.getSource() == this.vista.getBtnBorrar()) {
            int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
            for (int fila : filas_seleccionadas) {
                try {
                    this.agenda.borrarPersona(this.personas_en_tabla.get(fila));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }

            this.llenarTabla();

        } else if (e.getSource() == this.vista.getBtnConfiguracion()) {       	
        	this.ventanaConfiguracion = new VentanaConfiguracion(this);
        }

        //---------------------LOCALIDADES-----------------------------------
        else if (e.getSource() == this.vista.getBtnLocalidades()) {
            ventanaLocalidades = new VentanaLocalidades(this);
        }
        //-----------------------------------ETIQUETAS------------------------
        else if (e.getSource() == this.vista.getBtnEtiquetas()) {
            ventanaEtiquetas = new VentanaEtiquetas(this);
        }

        //-------------------------------------REPORTE------------------------
        else if (!isNull(this.vista) && e.getSource() == this.vista.getBtnReporte()) {

            ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
            reporte.mostrar();
        } else if (!isNull(this.ventanaPersona) && e.getSource() == this.ventanaPersona.getBtnAgregarPersona()) {
            boolean valObligatoy = validateObligatoryFields(ventanaPersona.getTxtNombre().getText(), "nombre") &&
                    validateObligatoryFields(ventanaPersona.getTxtTelefono().getText(), "telefono");

            if (valObligatoy && validateTextAgregar()) {
                PersonaDTO nuevaPersona = new PersonaDTO.PersonaBuilder

                        (0, this.ventanaPersona.getTxtNombre().getText(), ventanaPersona.getTxtTelefono().getText())

                        .withDireccion(this.ventanaPersona.getTxtCalle().getText(),
                                this.ventanaPersona.getTxtAltura().getText(),
                                this.ventanaPersona.getTxtDpto().getText(),
                                this.ventanaPersona.getTxtPiso().getText())

                        .withLocalidad((LocalidadDTO) this.ventanaPersona.getTxtLocalidad().getSelectedItem())

                        .withCumpleaños(this.ventanaPersona.getTxtCumpleanios())

                        .withContacto(this.ventanaPersona.getTxtMail().getText())

                        .withEtiqueta((EtiquetaDTO) this.ventanaPersona.getTxtTipo().getSelectedItem())

                        .build();

                this.agenda.agregarPersona(nuevaPersona);
                this.llenarTabla();
                this.ventanaPersona.dispose();
            }
        } else if (!isNull(this.ventanaPersonaEdicion) && e.getSource() == this.ventanaPersonaEdicion.getBtnEditarPersona()) {

            boolean valObligatory = validateObligatoryFields(ventanaPersonaEdicion.getTxtNombre().getText(), "nombre")
                    && validateObligatoryFields(ventanaPersonaEdicion.getTxtTelefono().getText(), "telefono");
            if (valObligatory && validateTextEdicion()) {
                PersonaDTO nuevaPersona = new PersonaDTO.PersonaBuilder

                        (ventanaPersonaEdicion.getId(), this.ventanaPersonaEdicion.getTxtNombre().getText(), ventanaPersonaEdicion.getTxtTelefono().getText())

                        .withDireccion(this.ventanaPersonaEdicion.getTxtCalle().getText(),
                                this.ventanaPersonaEdicion.getTxtAltura().getText(),
                                this.ventanaPersonaEdicion.getTxtDpto().getText(),
                                this.ventanaPersonaEdicion.getTxtPiso().getText())

                        .withLocalidad((LocalidadDTO) this.ventanaPersonaEdicion.getTxtLocalidad().getSelectedItem())

                        .withCumpleaños(this.ventanaPersonaEdicion.getTxtCumpleanios())

                        .withContacto(this.ventanaPersonaEdicion.getTxtMail().getText())

                        .withEtiqueta((EtiquetaDTO) this.ventanaPersonaEdicion.getTxtEtiqueta().getSelectedItem())

                        .build();

                this.agenda.editarPersona(nuevaPersona);
                this.llenarTabla();
                this.ventanaPersonaEdicion.dispose();
            }
        }

        //-----------BOTONES LOCALIDADES------------------
        else if (!isNull(this.ventanaLocalidades) && e.getSource() == this.ventanaLocalidades.getBtnAgregar()) {
            String aAgregar = ventanaLocalidades.getTxtNuevo().getText();
            LocalidadDTO localidad = new LocalidadDTO(0, aAgregar);
            localidadService.agregarLocalidad(localidad);
            ventanaLocalidades.cargarLocalidades();
            ventanaLocalidades.clearFields();
        } else if (!isNull(this.ventanaLocalidades) && e.getSource() == this.ventanaLocalidades.getBtnEliminar()) {

            try {
                LocalidadDTO aEliminar = (LocalidadDTO) ventanaLocalidades.getComboLocalidades().getSelectedItem();
                localidadService.eliminarLocalidad(aEliminar);
                ventanaLocalidades.cargarLocalidades();
                ventanaLocalidades.clearFields();
            } catch (SQLException e1) {
                Advertiser.advertencia("Acción prohibida, " +
                        "no se puede eliminar una Etiqueta en uso.");
            }
        }
        //-----------------BOTONES ETIQUETAS----------------

        else if (!isNull(this.ventanaEtiquetas) && e.getSource() == this.ventanaEtiquetas.getBtnAgregar()) {
            String aAgregar = ventanaEtiquetas.getTxtNuevo().getText();
            etiquetaService.agregarEtiqeta(new EtiquetaDTO(0, aAgregar));
            ventanaEtiquetas.cargarEtiquetas();
            ventanaEtiquetas.clearFields();
        } else if (!isNull(this.ventanaEtiquetas) && e.getSource() == this.ventanaEtiquetas.getBtnEliminar()) {
            try {
                EtiquetaDTO aEliminar = (EtiquetaDTO) ventanaEtiquetas.getComboEtiquetas().getSelectedItem();
                etiquetaService.eliminarEtiqueta(aEliminar);
                ventanaEtiquetas.cargarEtiquetas();
                ventanaEtiquetas.clearFields();
            } catch (SQLException e1) {
                Advertiser.advertencia("Acción prohibida, " +
                        "no se puede eliminar una Etiqueta en uso.");
            }
        }
    }

    private boolean validateTextAgregar() {
    	
    	boolean flagNombre = validator.validateOnlyLetters(ventanaPersona.getTxtNombre().getText());
    	
        boolean flagMail = validator.validateEmail(ventanaPersona.getTxtMail().getText())
                || validator.validateBlank(ventanaPersona.getTxtMail().getText());

        boolean flagTelefono = validator.validateOnlyNumber(ventanaPersona.getTxtTelefono().getText());

        if (!flagNombre) 			Advertiser.advertencia("Campo de nombre invalido, solo se permiten ingresar letras");
        if (!flagMail) 				Advertiser.advertencia("Por favor ingrese un mail invalido");
        if (!flagTelefono)			Advertiser.advertencia("Campo de telefono invalido, solo se permiten ingresar numeros");

        return flagNombre && flagMail && flagTelefono;
    }

    private boolean validateTextEdicion() {
    	
    	boolean flagNombre = validator.validateOnlyLetters(ventanaPersonaEdicion.getTxtNombre().getText());
        
    	boolean flagMail = validator.validateEmail(ventanaPersonaEdicion.getTxtMail().getText())
                || validator.validateBlank(ventanaPersonaEdicion.getTxtMail().getText());

        boolean flagTelefono = validator.validateOnlyNumber(ventanaPersonaEdicion.getTxtTelefono().getText());

        if (!flagNombre) 			Advertiser.advertencia("Campo de nombre invalido, solo se permiten ingresar letras");
        if (!flagMail) 				Advertiser.advertencia("Por favor ingrese un mail invalido");
        if (!flagTelefono)			Advertiser.advertencia("Campo de telefono invalido, solo se permiten ingresar numeros");

        return flagNombre && flagMail && flagTelefono;
    }

    private boolean validateObligatoryFields(String text, String campo) {
        if (validator.validateBlank(text)) {
            Advertiser.advertencia("Complete el campo obligatorio: " + campo);
            return false;
        }
        return true;
    }

    private boolean isNull(Object o) {
        return o == null;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public EtiquetaService getEtiquetaService() {
        return etiquetaService;
    }

    public LocalidadService getLocalidadService() {
        return localidadService;
    }
}
