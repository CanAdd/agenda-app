package CanAdd.agenda.presentacion.controlador;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoggingValidator {
    private static LoggingValidator ourInstance = new LoggingValidator();

    public static LoggingValidator getInstance() {
        return ourInstance;
    }

    private LoggingValidator() {
    }

    public boolean validateBlank(String text) {
        return match(text, "^$|\\s+");
    }

    public boolean validateEmail(String text) {
        return match(text, "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    public boolean validateOnlyNumber(String text) {
        return match(text, "^[0-9]*$");
    }

    public boolean validateOnlyLetters(String text) {
        return match(text, "^[a-zA-Z\\s]*$");
    }

    private boolean match(String text, String pat) {
        Pattern pattern = Pattern.compile(pat);
        Matcher matcher = pattern.matcher(text);


        return matcher.find();
    }

}
