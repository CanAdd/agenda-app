package CanAdd.agenda.presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JButton;

public class Vista {
    private JFrame frame;
    private JTable tablaPersonas;
    private JButton btnAgregar, btnEditar, btnBorrar, btnReporte, btnLocalidades, btnEtiquetas, btnConfiguracion;
    private DefaultTableModel modelPersonas;
    private String[] nombreColumnas = {"Nombre y apellido", "Telefono", "Calle", "Altura", "Piso",
            "Dpto.", "Localidad", "Mail", "Cumpleaños", "Etiqueta"};

    public Vista() {
        super();
        initialize();
    }


    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 880, 470);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 864, 421);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        JScrollPane spPersonas = new JScrollPane();
        spPersonas.setBounds(10, 11, 844, 331);
        panel.add(spPersonas);

        modelPersonas = new DefaultTableModel(null, nombreColumnas);
        tablaPersonas = new JTable(modelPersonas);

        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(modelPersonas);
        tablaPersonas.setRowSorter(sorter);

        tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(120);
        tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaPersonas.getColumnModel().getColumn(2).setPreferredWidth(100);
        tablaPersonas.getColumnModel().getColumn(5).setPreferredWidth(30);
        tablaPersonas.getColumnModel().getColumn(6).setPreferredWidth(100);
        tablaPersonas.getColumnModel().getColumn(7).setPreferredWidth(120);
        tablaPersonas.getColumnModel().getColumn(8).setPreferredWidth(70);
        tablaPersonas.getColumnModel().getColumn(9).setPreferredWidth(80);

        spPersonas.setViewportView(tablaPersonas);

        btnAgregar = new JButton("Agregar");
        btnAgregar.setBounds(620, 353, 112, 23);
        panel.add(btnAgregar);

        btnEditar = new JButton("Editar");
        btnEditar.setBounds(742, 353, 112, 23);
        panel.add(btnEditar);

        btnBorrar = new JButton("Borrar");
        btnBorrar.setBounds(620, 387, 112, 23);
        panel.add(btnBorrar);

        btnReporte = new JButton("Reporte");
        btnReporte.setBounds(742, 387, 112, 23);
        panel.add(btnReporte);

        btnLocalidades = new JButton("Localidades");
        btnLocalidades.setBounds(10, 353, 122, 23);
        panel.add(btnLocalidades);

        btnEtiquetas = new JButton("Etiquetas");
        btnEtiquetas.setBounds(142, 353, 122, 23);
        panel.add(btnEtiquetas);
        
        btnConfiguracion = new JButton("Configuracion de Base de Datos");
        btnConfiguracion.setBounds(10, 387, 254, 23);
        panel.add(btnConfiguracion);
    }

    public void show() {
        this.frame.setVisible(true);
    }

    public JButton getBtnAgregar() {
        return btnAgregar;
    }

    public JButton getBtnEditar() {
        return btnEditar;
    }

    public JButton getBtnBorrar() {
        return btnBorrar;
    }

    public JButton getBtnReporte() {
        return btnReporte;
    }

    public JButton getBtnLocalidades() {
        return btnLocalidades;
    }

    public JButton getBtnEtiquetas() {
        return btnEtiquetas;
    }
    
    public JButton getBtnConfiguracion() {
        return btnConfiguracion;
    }

    public DefaultTableModel getModelPersonas() {
        return modelPersonas;
    }

    public JTable getTablaPersonas() {
        return tablaPersonas;
    }

    public String[] getNombreColumnas() {
        return nombreColumnas;
    }

}
