package CanAdd.agenda.presentacion.vista;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.presentacion.controlador.Controlador;

import java.util.List;

public class VentanaLocalidades extends JFrame {
    private static final long serialVersionUID = 1L;
    private JTextField txtNuevo;
    private JComboBox<LocalidadDTO> comboLocalidad;
    private JButton btnAgregar, btnEliminar;
    private Controlador controlador;

    public VentanaLocalidades(Controlador controlador) {
        super();
        this.controlador = controlador;
        this.setTitle("Administracion de Localidades");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 343, 220);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        this.setTitle("Administrar Localidades");

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 307, 160);
        contentPane.add(panel);
        panel.setLayout(null);

        comboLocalidad = new JComboBox<>();
        comboLocalidad.setBounds(133, 26, 164, 20);
        panel.add(comboLocalidad);
        cargarLocalidades();

        inicializarLabels(panel);
        inicializarTextFields(panel);
        inicializarButtons(panel);

        this.setVisible(true);
    }

    private void inicializarButtons(JPanel panel) {
        btnAgregar = new JButton("Agregar");
        btnAgregar.addActionListener(this.controlador);
        btnAgregar.setBounds(10, 123, 89, 23);
        panel.add(btnAgregar);

        btnEliminar = new JButton("Eliminar");
        btnEliminar.addActionListener(this.controlador);
        btnEliminar.setBounds(208, 123, 89, 23);
        panel.add(btnEliminar);
    }

    private void inicializarLabels(JPanel panel) {
        JLabel lblAnterior = new JLabel("Localidad Existente");
        lblAnterior.setBounds(10, 29, 113, 14);
        panel.add(lblAnterior);

        JLabel lblNuevo = new JLabel("Localidad Nueva");
        lblNuevo.setBounds(10, 79, 113, 14);
        panel.add(lblNuevo);
    }

    private void inicializarTextFields(JPanel panel) {

        txtNuevo = new JTextField();
        txtNuevo.setBounds(133, 76, 164, 20);
        panel.add(txtNuevo);
        txtNuevo.setColumns(10);
    }

    public void cargarLocalidades() {
        comboLocalidad.removeAllItems();
        List<LocalidadDTO> localidades = controlador.getLocalidadService().readAllLocalidades();
        localidades.forEach(comboLocalidad::addItem);
    }

    public JTextField getTxtNuevo() {
        return txtNuevo;
    }

    public JComboBox<LocalidadDTO> getComboLocalidades() {
        return comboLocalidad;
    }

    public JButton getBtnAgregar() {
        return btnAgregar;
    }

    public JButton getBtnEliminar() {
        return btnEliminar;
    }

    public void clearFields() {
        comboLocalidad.setSelectedIndex(0);
        txtNuevo.setText("");
    }
}

