package CanAdd.agenda.presentacion.vista;

import CanAdd.agenda.dto.ConexionSettingsDTO;
import CanAdd.agenda.modelo.ConexionSettings;
import CanAdd.agenda.presentacion.controlador.Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class VentanaConfiguracion extends JFrame {
    private static final long serialVersionUID = 1L;
    private JTextField txtAdmin, txtPassword, txtIp, txtPort;
    private JButton btnGuardar, btnVolver;
    private Controlador controlador;

    public VentanaConfiguracion(Controlador controlador) {
        super();
        this.controlador = controlador;
        this.setTitle("Configuración de Base de Datos");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 335, 335);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        this.setTitle("Configuración de Base de Datos");

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 299, 275);
        contentPane.add(panel);
        panel.setLayout(null);

        inicializarLabels(panel);
        inicializarTextFields(panel);
        inicializarButtons(panel);

        this.setVisible(true);
    }

    public VentanaConfiguracion() {
        super();
        this.setTitle("Configuración de Base de Datos");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 335, 335);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        this.setTitle("Configuración de Base de Datos");

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 299, 275);
        contentPane.add(panel);
        panel.setLayout(null);

        inicializarLabels(panel);
        inicializarTextFields(panel);
        inicializarButtons(panel);

        this.setVisible(true);
    }

    private void cargarConfiguracion(){
        ConexionSettings settings = new ConexionSettings();
        ConexionSettingsDTO settingsDTO = settings.getSettings();

        txtAdmin.setText(settingsDTO.getAdmin());
        txtPassword.setText(settingsDTO.getPassword());
        txtIp.setText(settingsDTO.getIp());
        txtPort.setText(settingsDTO.getPort());
    }
    
    private void guardarConfiguracion(){
        ConexionSettings settings = new ConexionSettings();
        ConexionSettingsDTO settingsDTO = new ConexionSettingsDTO(txtAdmin.getText(), txtPassword.getText(), txtPort.getText(), txtIp.getText());
        settings.setSettings(settingsDTO); 
    }

    private void inicializarButtons(JPanel panel) {
    	
        btnGuardar = new JButton("Guardar y salir");
        btnGuardar.addActionListener(this.controlador);
        btnGuardar.setBounds(10, 237, 117, 23);
        panel.add(btnGuardar);
        btnGuardar.addActionListener(e -> {
            guardarConfiguracion();
            dispose();
        });
    
        btnVolver = new JButton("Cancelar");
        btnVolver.addActionListener(this.controlador);
        btnVolver.setBounds(167, 237, 117, 23);
        panel.add(btnVolver);
        btnVolver.addActionListener(e -> dispose());
    }

    private void inicializarTextFields(JPanel panel) {

        txtAdmin = new JTextField();
        txtAdmin.setBounds(112, 41, 164, 20);
        panel.add(txtAdmin);
        txtAdmin.setColumns(10);

        txtPassword = new JTextField();
        txtPassword.setBounds(112, 85, 164, 20);
        panel.add(txtPassword);
        txtPassword.setColumns(10);

        txtIp = new JTextField();
        txtIp.setBounds(112, 130, 164, 20);
        panel.add(txtIp);
        txtIp.setColumns(10);

        txtPort = new JTextField();
        txtPort.setBounds(112, 172, 164, 20);
        panel.add(txtPort);
        txtPort.setColumns(10);

        cargarConfiguracion();
    }

    private void inicializarLabels(JPanel panel) {
        JLabel lblAdmin = new JLabel("Admin:");
        lblAdmin.setBounds(10, 41, 92, 14);
        panel.add(lblAdmin);

        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setBounds(10, 88, 95, 14);
        panel.add(lblPassword);

        JLabel lblIp = new JLabel("Direccion IP:");
        lblIp.setBounds(10, 133, 92, 14);
        panel.add(lblIp);

        JLabel lblPort = new JLabel("Puerto:");
        lblPort.setBounds(10, 175, 92, 14);
        panel.add(lblPort);
    }

    public JTextField getTxtAdmin() {
        return txtAdmin;
    }

    public JTextField getTxtPassword() {
        return txtPassword;
    }

    public JTextField getTxtIp() {
        return txtIp;
    }

    public JTextField getTxtPort() {
        return txtPort;
    }

    public JButton getBtnEditar() {
        return btnGuardar;
    }

    public JButton getBtnCerrar() {
        return btnVolver;
    }
}
