package CanAdd.agenda.presentacion.vista;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import CanAdd.agenda.dto.EtiquetaDTO;
import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.presentacion.controlador.Controlador;

import java.util.List;

public class VentanaPersona extends JFrame {
    private Controlador controlador;
    private static final long serialVersionUID = 1L;
    private JTextField txtNombre, txtTelefono, txtCalle, txtAltura, txtPiso, txtDpto, txtMail;
    private JComboBox<LocalidadDTO> comboLocalidad;
    private JComboBox<EtiquetaDTO> comboTipo;
    private JComboBox<String> comboMes, comboDia;
    private JButton btnAgregarPersona;

    public VentanaPersona(Controlador controlador) {
        super();
        this.controlador = controlador;

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 400, 520);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        this.setTitle("Agregar Contacto");

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 364, 460);
        contentPane.add(panel);
        panel.setLayout(null);

        inicializarLabels(panel);
        inicializarTextFields(panel);
        inicializarComboBoxes(panel);

        btnAgregarPersona = new JButton("Agregar");
        btnAgregarPersona.addActionListener(this.controlador);
        btnAgregarPersona.setBounds(208, 426, 89, 23);
        panel.add(btnAgregarPersona);

        this.setVisible(true);
    }

    private void inicializarLabels(JPanel panel) {
        JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
        lblNombreYApellido.setBounds(10, 11, 113, 14);
        panel.add(lblNombreYApellido);

        JLabel lblTelfono = new JLabel("Tel\u00E9fono");
        lblTelfono.setBounds(10, 52, 113, 14);
        panel.add(lblTelfono);

        JLabel lblCalle = new JLabel("Calle");
        lblCalle.setBounds(10, 93, 113, 14);
        panel.add(lblCalle);

        JLabel lblAltura = new JLabel("Altura");
        lblAltura.setBounds(10, 134, 113, 14);
        panel.add(lblAltura);

        JLabel lblPiso = new JLabel("Piso");
        lblPiso.setBounds(10, 175, 113, 14);
        panel.add(lblPiso);

        JLabel lblDpto = new JLabel("Dpto");
        lblDpto.setBounds(10, 216, 113, 14);
        panel.add(lblDpto);

        JLabel lblLocalidad = new JLabel("Localidad");
        lblLocalidad.setBounds(10, 257, 113, 14);
        panel.add(lblLocalidad);

        JLabel lblMail = new JLabel("Mail");
        lblMail.setBounds(10, 298, 113, 14);
        panel.add(lblMail);

        JLabel lblTipo = new JLabel("Tipo");
        lblTipo.setBounds(10, 339, 113, 14);
        panel.add(lblTipo);

        JLabel lblCumpleanios = new JLabel("Cumpleaños");
        lblCumpleanios.setBounds(10, 380, 113, 14);
        panel.add(lblCumpleanios);

        JLabel lblDia = new JLabel("Dia:");
        lblDia.setHorizontalAlignment(SwingConstants.CENTER);
        lblDia.setBounds(250, 380, 47, 14);
        panel.add(lblDia);

        JLabel lblMes = new JLabel("Mes:");
        lblMes.setHorizontalAlignment(SwingConstants.CENTER);
        lblMes.setBounds(133, 380, 44, 14);
        panel.add(lblMes);
    }

    private void inicializarTextFields(JPanel panel) {
        txtNombre = new JTextField();
        txtNombre.setBounds(133, 8, 221, 20);
        panel.add(txtNombre);
        txtNombre.setColumns(10);

        txtTelefono = new JTextField();
        txtTelefono.setBounds(133, 49, 221, 20);
        panel.add(txtTelefono);
        txtTelefono.setColumns(10);

        txtCalle = new JTextField();
        txtCalle.setBounds(133, 90, 221, 20);
        panel.add(txtCalle);
        txtCalle.setColumns(10);

        txtAltura = new JTextField();
        txtAltura.setBounds(133, 131, 221, 20);
        panel.add(txtAltura);
        txtAltura.setColumns(10);

        txtPiso = new JTextField();
        txtPiso.setBounds(133, 172, 221, 20);
        panel.add(txtPiso);
        txtPiso.setColumns(10);

        txtDpto = new JTextField();
        txtDpto.setBounds(133, 213, 221, 20);
        panel.add(txtDpto);
        txtDpto.setColumns(10);

        txtMail = new JTextField();
        txtMail.setBounds(133, 295, 221, 20);
        panel.add(txtMail);
        txtMail.setColumns(10);

    }

    private void inicializarComboBoxes(JPanel panel) {

        comboLocalidad = new JComboBox<>();
        comboLocalidad.setBounds(133, 254, 221, 20);
        panel.add(comboLocalidad);
        cargarLocalidades(comboLocalidad);

        comboTipo = new JComboBox<>();
        comboTipo.setBounds(133, 336, 221, 20);
        panel.add(comboTipo);
        cargarEtiquetas(comboTipo);

        comboDia = new JComboBox<>();
        comboDia.setBounds(304, 377, 50, 20);
        panel.add(comboDia);
        cargarCamposDia(comboDia, 31);

        comboMes = new JComboBox<>();
        comboMes.setBounds(187, 377, 50, 20);
        panel.add(comboMes);
        cargarCamposMes(comboMes);
        comboMes.addActionListener(e -> actualizarDiasMes(comboDia));
    }

    private void cargarLocalidades(JComboBox<LocalidadDTO> cBox) {
        List<LocalidadDTO> localidades = controlador.getLocalidadService().readAllLocalidades();

        localidades.forEach(cBox::addItem);

    }

    private void cargarEtiquetas(JComboBox<EtiquetaDTO> cBox) {
        List<EtiquetaDTO> etiquetas = controlador.getEtiquetaService().readAllEtiquetas();
        etiquetas.forEach(cBox::addItem);

    }

    private void cargarCamposMes(JComboBox<String> cBox) {

        cBox.addItem("01");
        cBox.addItem("02");
        cBox.addItem("03");
        cBox.addItem("04");
        cBox.addItem("05");
        cBox.addItem("06");
        cBox.addItem("07");
        cBox.addItem("08");
        cBox.addItem("09");
        cBox.addItem("10");
        cBox.addItem("11");
        cBox.addItem("12");
    }

    private void actualizarDiasMes(JComboBox<String> cBox) {

        cBox.removeAllItems();
        switch (comboMes.getSelectedIndex() + 1) {

            case 1:
                cargarCamposDia(cBox, 31);
                break;
            case 2:
                cargarCamposDia(cBox, 29);
                break;
            case 3:
                cargarCamposDia(cBox, 31);
                break;
            case 4:
                cargarCamposDia(cBox, 30);
                break;
            case 5:
                cargarCamposDia(cBox, 31);
                break;
            case 6:
                cargarCamposDia(cBox, 30);
                break;
            case 7:
                cargarCamposDia(cBox, 31);
                break;
            case 8:
                cargarCamposDia(cBox, 31);
                break;
            case 9:
                cargarCamposDia(cBox, 30);
                break;
            case 10:
                cargarCamposDia(cBox, 31);
                break;
            case 11:
                cargarCamposDia(cBox, 30);
                break;
            case 12:
                cargarCamposDia(cBox, 31);
                break;
        }
    }

    private void cargarCamposDia(JComboBox<String> cBox, int diasMes) {

        for (Integer i = 1; i < 10; i++) {
            cBox.addItem("0" + i.toString());
        }

        for (Integer j = 10; j <= diasMes; j++) {
            cBox.addItem(j.toString());
        }
    }

    public JTextField getTxtNombre() {
        return txtNombre;
    }

    public JTextField getTxtTelefono() {
        return txtTelefono;
    }

    public JTextField getTxtCalle() {
        return txtCalle;
    }

    public JTextField getTxtAltura() {
        return txtAltura;
    }

    public JTextField getTxtPiso() {
        return txtPiso;
    }

    public JTextField getTxtDpto() {
        return txtDpto;
    }

    public String getTxtCumpleanios() {
        return comboDia.getSelectedItem().toString() + "/" + comboMes.getSelectedItem().toString();
    }

    public JComboBox<LocalidadDTO> getTxtLocalidad() {
        return comboLocalidad;
    }

    public JTextField getTxtMail() {
        return txtMail;
    }

    public JComboBox<EtiquetaDTO> getTxtTipo() {
        return comboTipo;
    }

    public JButton getBtnAgregarPersona() {
        return btnAgregarPersona;
    }

}

