package CanAdd.agenda.presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import CanAdd.agenda.modelo.Agenda;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import CanAdd.agenda.dto.PersonaDTO;

public class ReporteAgenda {
    private JasperPrint reporteLleno;

    private Agenda agenda;

    private final static String jasperTemplate = "reportes" + File.separatorChar + "reportes.jrxml";
    private final static String reporteLocation = "reportes" + File.separatorChar + "reportes.jasper";

    //Recibe la lista de personas para armar el reporte
    public ReporteAgenda(List<PersonaDTO> personas) {
        agenda = new Agenda();
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        parametersMap.put("Total", personas.size());
        try {
            if (!new File(reporteLocation).exists()) JasperCompileManager.compileReportToFile(
                    jasperTemplate,
                    reporteLocation);

            JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile("reportes/reportes.jasper");
            this.reporteLleno = JasperFillManager.fillReport(reporte, parametersMap,
                    new JRBeanCollectionDataSource(agenda.sortByHroscopo(personas)));
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }

    public void mostrar() {
        JasperViewer reporteViewer = new JasperViewer(this.reporteLleno, false);
        reporteViewer.setVisible(true);
    }

}	