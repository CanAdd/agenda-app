package CanAdd.agenda.persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import CanAdd.agenda.dto.EtiquetaDTO;
import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.persistencia.conexion.Conexion;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;
import CanAdd.agenda.dto.PersonaDTO;

public class PersonaDAOImpl implements DAO<PersonaDTO> {

    private static String insertPersona="INSERT INTO Personas VALUES(?, ?, ?, ?, ?)";
    private static String deletePersona="DELETE FROM Personas WHERE idPersona = ?";
    private static String updatePersona="UPDATE Personas SET Nombre = ?, cumpleAnios = ?, idEtiqueta = ?, " +
            "idLocalidad = ? WHERE idPersona = ?";
    private static String readAllPersona="SELECT * FROM Personas p, Direcciones d, Etiquetas e, MediosContactos mc," +
            " Localidades l WHERE p.idPersona = d.idPersona AND mc.idPersona = p.idPersona AND" +
            " e.idEtiqueta = p.idEtiqueta AND l.idLocalidad = p.idLocalidad";

    private static String insertDireccion="INSERT INTO Direcciones VALUES(?, ?, ?, ?, ?)";
    private static String deleteDireccion="DELETE FROM Direcciones WHERE idPersona = ?";
    private static String updateDireccion="UPDATE Direcciones SET calle = ?, numero = ?, depto = ?," +
            " piso = ? WHERE idPersona = ?";

    private static String insertMedioContacto="INSERT INTO MediosContactos VALUES(?, ?, ?)";
    private static String deleteMedioContacto="DELETE FROM MediosContactos WHERE idPersona = ?";
    private static String updateMedioContacto="UPDATE MediosContactos SET telefono = ?, eMail = ? WHERE idPersona = ?";

    private static final Conexion conexion = Conexion.getConexion();

    public boolean insert(PersonaDTO persona) {
        PreparedStatement statement;
        try {
            int flag = 0;

            statement = conexion.getSQLConexion().prepareStatement(insertPersona);
            statement.setInt(1, persona.getIdPersona());
            statement.setString(2, persona.getNombre());
            statement.setString(3, persona.getCumpleaños());
            statement.setInt(4, persona.getEtiqueta().getIdEtiqueta());
            statement.setInt(5, persona.getLocalidad().getIdLocalidad());
            flag += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(insertDireccion);
            statement.setInt(1, persona.getIdPersona());
            statement.setString(2, persona.getDireccion().getCalle());
            statement.setString(3, persona.getDireccion().getNumero());
            statement.setString(4, persona.getDireccion().getDepto());
            statement.setString(5, persona.getDireccion().getPiso());
            flag += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(insertMedioContacto);
            statement.setInt(1, persona.getIdPersona());
            statement.setString(2, persona.getMediosDeContacto().getTelefono());
            statement.setString(3, persona.getMediosDeContacto().getEmail());
            flag += statement.executeUpdate();

            if (flag > 0) //Si se ejecutó devuelvo true
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    public boolean delete(PersonaDTO persona_a_eliminar) {
        PreparedStatement statement;
        int chequeoUpdate = 0;
        try {
            statement = conexion.getSQLConexion().prepareStatement(deleteDireccion);
            statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
            chequeoUpdate += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(deleteMedioContacto);
            statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
            chequeoUpdate += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(deletePersona);
            statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
            chequeoUpdate += statement.executeUpdate();

            if (chequeoUpdate > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    public boolean upgrade(PersonaDTO persona_a_actualizar) {
        PreparedStatement statement;
        try {
            int flag = 0;

            statement = conexion.getSQLConexion().prepareStatement(updatePersona);
            statement.setString(1, persona_a_actualizar.getNombre());
            statement.setString(2, persona_a_actualizar.getCumpleaños());
            statement.setInt(3, persona_a_actualizar.getEtiqueta().getIdEtiqueta());
            statement.setInt(4, persona_a_actualizar.getLocalidad().getIdLocalidad());
            statement.setInt(5, persona_a_actualizar.getIdPersona());
            flag += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(updateDireccion);
            statement.setString(1, persona_a_actualizar.getDireccion().getCalle());
            statement.setString(2, persona_a_actualizar.getDireccion().getNumero());
            statement.setString(3, persona_a_actualizar.getDireccion().getDepto());
            statement.setString(4, persona_a_actualizar.getDireccion().getPiso());
            statement.setInt(5, persona_a_actualizar.getIdPersona());
            flag += statement.executeUpdate();

            statement = conexion.getSQLConexion().prepareStatement(updateMedioContacto);
            statement.setString(1, persona_a_actualizar.getMediosDeContacto().getTelefono());
            statement.setString(2, persona_a_actualizar.getMediosDeContacto().getEmail());
            statement.setInt(3, persona_a_actualizar.getIdPersona());
            flag += statement.executeUpdate();

            if (flag > 0) //Si se ejecutó devuelvo true
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    public List<PersonaDTO> readAll() {
        PreparedStatement statement;
        ResultSet resultSet; //Guarda el resultado de la query
        ArrayList<PersonaDTO> personas = new ArrayList<>();
        try {
            statement = conexion.getSQLConexion().prepareStatement(readAllPersona);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                personas.add(new PersonaDTO.PersonaBuilder
                        (resultSet.getInt("idPersona"), resultSet.getString("Nombre"),
                                resultSet.getString("Telefono")).
                        withCumpleaños(resultSet.getString("cumpleAnios")).
                        withContacto(resultSet.getString("eMail")).
                        withEtiqueta(new EtiquetaDTO(resultSet.getInt("idEtiqueta"),
                                resultSet.getString("relacion"))).
                        withDireccion(resultSet.getString("calle"), resultSet.getString("numero"),
                                resultSet.getString("depto"), resultSet.getString("piso")).
                        withLocalidad(new LocalidadDTO(resultSet.getInt("idLocalidad"),
                                resultSet.getString("localidad"))).
                        build());
                System.out.println("this is " + personas);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return personas;
    }

}
