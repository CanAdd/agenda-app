package CanAdd.agenda.persistencia.dao.mysql;

import CanAdd.agenda.dto.LocalidadDTO;
import CanAdd.agenda.persistencia.conexion.Conexion;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LocalidadDAOImpl implements DAO<LocalidadDTO> {

    private static String insertLocalidad = "INSERT INTO Localidades VALUES(?, ?)";
    private static String deleteLocalidad = "DELETE FROM Localidades WHERE idLocalidad= ?";
    private static String updateLocalidad = "UPDATE Localidades SET localidad = ? WHERE idLocalidad = ?";
    private static String readAllLocalidad = "SELECT * FROM Localidades";


    private static final Conexion conexion = Conexion.getConexion();

    @Override
    public boolean insert(LocalidadDTO localidadDTO) {
        PreparedStatement statement;
        int chequeoUpdate;
        try {

            statement = conexion.getSQLConexion().prepareStatement(insertLocalidad);
            statement.setInt(1, localidadDTO.getIdLocalidad());
            statement.setString(2, localidadDTO.getLocalidad());

            chequeoUpdate = statement.executeUpdate();
            if (chequeoUpdate > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(LocalidadDTO localidadDTO) throws SQLException {
        PreparedStatement statement;
        int chequeoUpdate;

        statement = conexion.getSQLConexion().prepareStatement(deleteLocalidad);
        statement.setInt(1, localidadDTO.getIdLocalidad());

        chequeoUpdate = statement.executeUpdate();
        if (chequeoUpdate > 0)
            return true;

        conexion.cerrarConexion();
        return false;
    }

    @Override
    public boolean upgrade(LocalidadDTO localidadDTO) {
        PreparedStatement statement;
        int chequeoUpdate;
        try {
            statement = conexion.getSQLConexion().prepareStatement(updateLocalidad);
            statement.setString(1, localidadDTO.getLocalidad());
            statement.setInt(2, localidadDTO.getIdLocalidad());

            chequeoUpdate = statement.executeUpdate();
            if (chequeoUpdate > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    @Override
    public List<LocalidadDTO> readAll() {
        List<LocalidadDTO> ret = new ArrayList<>();
        ResultSet resultSet;
        PreparedStatement statement;

        try {
            statement = conexion.getSQLConexion().prepareStatement(readAllLocalidad);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ret.add(new LocalidadDTO(resultSet.getInt("idLocalidad"),
                        resultSet.getString("localidad")));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }

        return ret;
    }

}
