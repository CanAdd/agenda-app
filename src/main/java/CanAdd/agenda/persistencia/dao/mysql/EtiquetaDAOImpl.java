package CanAdd.agenda.persistencia.dao.mysql;

import CanAdd.agenda.dto.EtiquetaDTO;
import CanAdd.agenda.persistencia.conexion.Conexion;
import CanAdd.agenda.persistencia.dao.interfaz.DAO;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EtiquetaDAOImpl implements DAO<EtiquetaDTO> {

    private static String insertEtiqueta = "INSERT INTO Etiquetas VALUES(?, ?)";
    private static String deleteEtiqueta = "DELETE FROM Etiquetas WHERE idEtiqueta = ?";
    private static String updateEtiqueta = "UPDATE Etiquetas SET relacion = ? WHERE idEtiqueta = ?";
    private static String readAllEtiqueta = "SELECT * FROM Etiquetas";

    private static final Conexion conexion = Conexion.getConexion();

    @Override
    public boolean insert(EtiquetaDTO etiquetaDTO) {
        PreparedStatement statement;
        int chequeoUpdate;
        try {
            statement = conexion.getSQLConexion().prepareStatement(insertEtiqueta);
            statement.setInt(1, etiquetaDTO.getIdEtiqueta());
            statement.setString(2, etiquetaDTO.getRelacion());

            chequeoUpdate = statement.executeUpdate();
            if (chequeoUpdate > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(EtiquetaDTO etiquetaDTO) throws SQLException {
        PreparedStatement statement;
        int chequeoUpdate;

        statement = conexion.getSQLConexion().prepareStatement(deleteEtiqueta);
        statement.setInt(1, etiquetaDTO.getIdEtiqueta());

        chequeoUpdate = statement.executeUpdate();
        if (chequeoUpdate > 0)
            return true;

        conexion.cerrarConexion();
        return false;
    }

    @Override
    public boolean upgrade(EtiquetaDTO etiquetaDTO) {
        PreparedStatement statement;
        int chequeoUpdate;
        try {
            statement = conexion.getSQLConexion().prepareStatement(updateEtiqueta);
            statement.setInt(2, etiquetaDTO.getIdEtiqueta());
            statement.setString(1, etiquetaDTO.getRelacion());

            chequeoUpdate = statement.executeUpdate();
            if (chequeoUpdate > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }
        return false;
    }

    @Override
    public List<EtiquetaDTO> readAll() {
        List<EtiquetaDTO> ret = new ArrayList<>();
        ResultSet resultSet;
        PreparedStatement statement;

        try {
            statement = conexion.getSQLConexion().prepareStatement(readAllEtiqueta);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ret.add(new EtiquetaDTO(resultSet.getInt("idEtiqueta"),
                        resultSet.getString("relacion")));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexion.cerrarConexion();
        }

        return ret;
    }

}
