package CanAdd.agenda.persistencia.dao.interfaz;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

public interface DAO<T> {
    public boolean insert(T t);

    public boolean delete(T t) throws SQLIntegrityConstraintViolationException, SQLException;

    public boolean upgrade(T t);

    public List<T> readAll();
}
