package CanAdd.agenda.persistencia.conexion;

import CanAdd.agenda.dto.ConexionSettingsDTO;
import CanAdd.agenda.modelo.ConexionSettings;
import com.mysql.jdbc.JDBC4Connection;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

public class Conexion {
    private static Conexion instancia;
    private Connection conexion;

    private Conexion() {
        try {
            ConexionSettings conexionSettings = new ConexionSettings();
            ConexionSettingsDTO settings = conexionSettings.getSettings();
            Class.forName("com.mysql.jdbc.Driver");

            conexion = DriverManager.getConnection("jdbc:mysql://" +
                            settings.getIp() +
                            ":" +
                            settings.getPort() +
                            "/tpi_g9",
                    settings.getAdmin(), settings.getPassword());
            System.out.println("Conexion exitosa");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Conexion fallida");
        }
    }

    public static Conexion getConexion() {
        if (instancia == null) {
            instancia = new Conexion();
        }
        return instancia;
    }

    public Connection getSQLConexion() {
        return conexion;
    }

    public void cerrarConexion() {
        instancia = null;
    }
}
